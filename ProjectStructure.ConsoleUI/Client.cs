﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.DAL;
using ProjectStructure.Model;
using ProjectStructure.Model.DTO;
using ProjectStructure.WebApi.DTO;

namespace ProjectStructure.ConsoleUI {
    public class Client {
        private Dictionary<UiActions, string> ActionsDictionary;

        public void Start() {
            InitActions();
            GreatUser();
            Execute();
        }

        private void Execute() {
            ShowActions();
            UiActions selectedAction = PromptUser();
            ExecuteAction(selectedAction);
            AskToContinue();
        }

        private void AskToContinue() {
            Console.WriteLine("\nDo you want to continue: yes (Y) \\ no (N)?");
            var key = Console.ReadKey().KeyChar;
            if (key == 'y' || key == 'Y') {
                Console.WriteLine();
                Execute();
            } else if (key == 'n' || key == 'N') {
                return;
            } else {
                Console.WriteLine("Please select right choice!");
                AskToContinue();
            }
        }

        private void ShowActions() {
            Console.WriteLine("\nQuery master actions:");
            for (int i = 0; i < ActionsDictionary.Count; i++) {
                Console.WriteLine($"{i + 1} - {ActionsDictionary.ElementAt(i).Value}");
            }
        }

        private void GreatUser() {
            Console.WriteLine("Query master greats you!");
        }

        private UiActions PromptUser() {
            Console.WriteLine("\nExecute an action:");
            var input = Console.ReadLine();
            if (Int32.TryParse(input, out int actionId) && actionId > 0 && actionId <= ActionsDictionary.Count)
                return (UiActions)actionId;
            else {
                Console.WriteLine("Please enter valid action.");
                PromptUser();
            }

            return UiActions.NoAction;
        }

        private void InitActions() {
            ActionsDictionary = new Dictionary<UiActions, string> {
                { UiActions.LoadProjects, "Load all projects" },
                { UiActions.LoadProject, "Load project with id provided." },
                { UiActions.TasksPerUserProject, "Load tasks for user project by User id provided." },
                { UiActions.TasksPerUserWithNameLengthUpTo45Symbols, "Load tasks with name up to 45 symbols for user project by User id provided." },
            };
        }

        private void ExecuteAction(UiActions selectedAction) {
            string input;
            int id;

            switch (selectedAction) {
                case UiActions.LoadProjects:
                    var projects = GetProjects().Result;
                    Console.WriteLine(String.Join(", ", projects));
                    break;

                case UiActions.LoadProject:
                    Console.WriteLine("Please enter project id (number):");
                    input = Console.ReadLine();
                    if (!Int32.TryParse(input, out id)) {
                        Console.WriteLine("Project id is not valid. Please reenter");
                        break;
                    }

                    var project = GetProject(id).Result;
                    Console.WriteLine(String.Join(", ", project));
                    break;

                case UiActions.TasksPerUserProject:
                    Console.WriteLine("Please enter user id:");
                    input = Console.ReadLine();
                    if (!Int32.TryParse(input, out id)) {
                        Console.WriteLine("Project id is not valid. Please reenter");
                        break;
                    }

                    var tasksPerUserProject = GetTasksPerUserProject(id).Result;
                    Console.WriteLine(String.Join(", ", tasksPerUserProject));
                    break;        
                
                case UiActions.TasksPerUserWithNameLengthUpTo45Symbols:
                    Console.WriteLine("Please enter user id:");
                    input = Console.ReadLine();
                    if (!Int32.TryParse(input, out id)) {
                        Console.WriteLine("Project id is not valid. Please reenter");
                        break;
                    }

                    var tasksPerUserProjectWithNameUpTo45Symbols = GetTasksPerUserWithNameLengthUpTo45Symbols(id).Result;
                    Console.WriteLine(String.Join(", ", tasksPerUserProjectWithNameUpTo45Symbols));
                    break;

                default:
                    Console.WriteLine("Such action is not supported");
                    break;
            }
        }

        public async Task<List<BsaTaskDto>> GetTasksPerUserWithNameLengthUpTo45Symbols(int id) {
            var json = await BsaHttpApiClient.GetTasksPerUserWithNameLengthUpTo45SymbolsAsync(id);
            var tasksPerUserProject = JsonConvert.DeserializeObject<List<BsaTaskDto>>(json);
            return tasksPerUserProject;
        }
        public async Task<List<TasksPerUserProjectDto>> GetTasksPerUserProject(int id) {
            var json = await BsaHttpApiClient.GetTasksPerUserProjectDtoAsync(id);
            var tasksPerUserProject = JsonConvert.DeserializeObject<List<TasksPerUserProjectDto>>(json);
            return tasksPerUserProject;
        }

        public async Task<List<TaskStateDto>> GetTaskStates() {
            var json = await BsaHttpApiClient.GetTaskStatesAsync();
            List<TaskStateDto> tastStates = JsonConvert.DeserializeObject<List<TaskStateDto>>(json);
            return tastStates;
        }

        public async Task<List<TeamDto>> GetTeams() {
            var json = await BsaHttpApiClient.GetTeamsAsync();
            List<TeamDto> teams = JsonConvert.DeserializeObject<List<TeamDto>>(json);
            return teams;
        }

        public async Task<List<UserDto>> GetUsers() {
            var json = await BsaHttpApiClient.GetUsersAsync();
            List<UserDto> users = JsonConvert.DeserializeObject<List<UserDto>>(json);
            return users;
        }

        public async Task<List<ProjectDto>> GetProjects() {
            var json = await BsaHttpApiClient.GetProjectsAsync();
            List<ProjectDto> projects = JsonConvert.DeserializeObject<List<ProjectDto>>(json);
            return projects;
        }

        public async Task<ProjectDto> GetProject(int id) {
            var json = await BsaHttpApiClient.GetProjectAsync(id);
            ProjectDto projects = JsonConvert.DeserializeObject<ProjectDto>(json);
            return projects;
        }

        public async Task<List<BsaTaskDto>> GetTasks() {
            var json = await BsaHttpApiClient.GetTasksAsync();
            List<BsaTaskDto> tasks = JsonConvert.DeserializeObject<List<BsaTaskDto>>(json);
            return tasks;
        }
    }
}