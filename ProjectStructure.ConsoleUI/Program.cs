﻿namespace ProjectStructure.ConsoleUI {
    class Program {
        static void Main(string[] args) {
            var client = new Client();
            client.Start();
        }
    }
}