﻿namespace ProjectStructure.DAL {
    public enum UiActions {
        NoAction = 0,
        LoadProjects = 1,
        LoadProject = 2,
        TasksPerUserProject = 3,
        TasksPerUserWithNameLengthUpTo45Symbols = 4,
        TasksPerUserFinishedIn2020 = 5,
        CommandsWithUsersOlder10Years = 6,
        UsersSortedByFirstNameAndTaskNameLength = 7,
        UserAndProjectsDetails = 8,
        ProjectWithDetails = 9
    }
}