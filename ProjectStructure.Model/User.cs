﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.Model {
    public class User : Entity {
        //schema
        // "id": 0,
        // "firstName": "string",
        // "lastName": "string",
        // "email": "string",
        // "birthday": "2020-07-06T13:23:49.629Z",
        // "registeredAt": "2020-07-06T13:23:49.629Z",
        // "teamId": 0
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }

        public List<BsaTask> Tasks { get; set; }
    }
}