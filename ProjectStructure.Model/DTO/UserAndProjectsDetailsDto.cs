﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructure.WebApi.DTO;

namespace ProjectStructure.Model.DTO {
    public class UserAndProjectsDetailsDto {
        public UserAndProjectsDetailsDto(UserDto userDto, ProjectDto projectDto, int totalTasksCount, int notFinishedTasksCount, BsaTaskDto longestTask) {
            UserDto = userDto;
            ProjectDto = projectDto;
            TotalTasksCount = totalTasksCount;
            NotFinishedTasksCount = notFinishedTasksCount;
            LongestTask = longestTask;
        }

        public UserDto UserDto { get; set; }
        public ProjectDto ProjectDto { get; set; }
        public int TotalTasksCount { get; set; }
        public int NotFinishedTasksCount { get; set; }
        public BsaTaskDto LongestTask { get; set; }

        public override string ToString() {
            return $"{nameof(UserDto)}: {UserDto}, {nameof(ProjectDto)}: {ProjectDto}, {nameof(TotalTasksCount)}: {TotalTasksCount}, {nameof(NotFinishedTasksCount)}: {NotFinishedTasksCount}, {nameof(LongestTask)}: {LongestTask}";
        }
    }
}