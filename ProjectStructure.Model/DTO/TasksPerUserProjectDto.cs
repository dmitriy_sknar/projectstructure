﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructure.WebApi.DTO;

namespace ProjectStructure.Model.DTO {
    public class TasksPerUserProjectDto {
        public ProjectDto ProjectDto { get; set; }
        public int TasksCount { get; set; }

        public override string ToString() {
            return $"{nameof(ProjectDto)}: {ProjectDto}, {nameof(TasksCount)}: {TasksCount}";
        }
    }
}