﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Model.DTO {
    public class TasksPerUserFinishedIn2020Dto {
        public TasksPerUserFinishedIn2020Dto(int id, string name) {
            Id = id;
            Name = name;
        }
        public int Id { get; set; }
        public string Name { get; set; }

        public override string ToString() {
            return $"{nameof(Id)}: {Id}, {nameof(Name)}: {Name}";
        }
    }
}