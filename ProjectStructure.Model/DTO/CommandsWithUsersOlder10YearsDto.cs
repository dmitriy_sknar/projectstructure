﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Model.DTO {
    public class CommandsWithUsersOlder10YearsDto {
        public CommandsWithUsersOlder10YearsDto(int id, string name, IEnumerable<UserDto> usersDtos) {
            Id = id;
            Name = name;
            UsersDtos = usersDtos;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDto> UsersDtos { get; set; }

        public override string ToString() {
            return $"{nameof(Id)}: {Id}, {nameof(Name)}: {Name}, {nameof(UsersDtos)}: {UsersDtos}";
        }
    }
}