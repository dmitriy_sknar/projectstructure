﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ProjectStructure.Model.DTO {
    public class TeamDto {

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public List<User> TeamMates { get; set; }

        public override string ToString() {
            return $"{nameof(Id)}: {Id}, {nameof(Name)}: {Name}, {nameof(CreatedAt)}: {CreatedAt}, {nameof(TeamMates)}: {TeamMates}";
        }
    }
}