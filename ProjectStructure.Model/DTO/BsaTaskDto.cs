﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructure.WebApi.DTO;

namespace ProjectStructure.Model.DTO {
    public class BsaTaskDto {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }

        public int State { get; set; }
        public TaskStateDto TaskState { get; set; }

        public int ProjectId { get; set; }
        public ProjectDto Project { get; set; }

        public int PerformerId { get; set; }
        public UserDto Performer { get; set; }

        public override string ToString() {
            return
                $"{nameof(Id)}: {Id}, {nameof(Name)}: {Name}, {nameof(Description)}: {Description}, {nameof(CreatedAt)}: {CreatedAt}, {nameof(FinishedAt)}: {FinishedAt}, {nameof(State)}: {State}, {nameof(TaskState)}: {TaskState}, {nameof(ProjectId)}: {ProjectId}, {nameof(Project)}: {Project}, {nameof(PerformerId)}: {PerformerId}, {nameof(Performer)}: {Performer}";
        }
    }
}