﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Model.DTO {
    public class TaskStateDto {
        public int Id { get; set; }

        public string Value { get; set; }

        public override string ToString() {
            return $"{nameof(Id)}: {Id}, {nameof(Value)}: {Value}";
        }
    }
}