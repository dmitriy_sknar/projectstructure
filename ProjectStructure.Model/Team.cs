﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.Model {
    public class Team : Entity {
        //schema:
        // "id": 0,
        // "name": "string",
        // "createdAt": "2020-07-06T13:20:04.634Z"
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public List<User> TeamMates { get; set; }
    }
}