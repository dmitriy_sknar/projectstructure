﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.Model {
    public class Project : Entity {
        //schema:
        // "id": 0,
        // "name": "string",
        // "description": "string",
        // "createdAt": "2020-07-06T12:04:35.449Z",
        // "deadline": "2020-07-06T12:04:35.449Z",
        // "authorId": 0,
        // "teamId": 0

        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime DeadLine { get; set; }

        public int AuthorId { get; set; }
        public User Author { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }

        public List<BsaTask> Tasks;
    }
}