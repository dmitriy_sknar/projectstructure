﻿namespace ProjectStructure.Model {
    public class TaskState : Entity {
        //schema 
        // "id": 0,
        // "value": "string"

        public string Value { get; set; }
    }
}