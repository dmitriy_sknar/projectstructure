﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Model;
using ProjectStructure.Model.DTO;
using ProjectStructure.WebApi.DTO;
using ProjectStructure.WebApi.Services;

namespace ProjectStructure.WebApi.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase {
        private IRepository<Project> _repo;
        private IMapper _mapper;

        public ProjectsController(IRepository<Project> repo, IMapper mapper) {
            _repo = repo;
            _mapper = mapper;
        }

        // GET: api/<ProjectsController>
        [HttpGet]
        public IEnumerable<ProjectDto> Get() {
            var list = _repo.Get();
            var dtoList = _mapper.Map<IEnumerable<ProjectDto>>(list);
            return dtoList;
        }

        // GET api/<ProjectsController>/5
        [HttpGet("{id}")]
        public ActionResult<ProjectDto> Get(int id) {
            var project = _repo.Get(x => x.Id == id).FirstOrDefault();
            if (project != null) {
                var dto = _mapper.Map<ProjectDto>(project);
                return Ok(dto);
            }

            return NotFound();
        }

        // POST api/<ProjectsController>
        [HttpPost]
        public void Post([FromBody] ProjectDto dto) {
            _repo.Create(_mapper.Map<Project>(dto));
        }

        // PUT api/<ProjectsController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] ProjectDto dto) {
            var project = _repo.Get(x => x.Id == id).FirstOrDefault();
            if (project != null) {
                project = _mapper.Map<Project>(dto);
                _repo.Update(project);
            }
        }

        // DELETE api/<ProjectsController>/5
        [HttpDelete("{userId}")]
        public void Delete(int userId) {
            _repo.Delete(new Project() {
                Id = userId
            });
        }

        //1. Получить кол-во тасков у проекта конкретного пользователя (по id) (словарь, где ключом будет проект, а значением кол-во тасков).
        // GET api/<ProjectsController>/TasksPerUserProject/5
        [HttpGet("TasksPerUserProject/{userId}")]
        public IEnumerable<TasksPerUserProjectDto> TasksPerUserProject(int userId) {
            Dictionary<Project, int> dict = _repo.Get().Where(p => p.AuthorId == userId)
                .Select(p => new KeyValuePair<Project, int>(p, p.Tasks.Count))
                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

            return dict.Select(kvp => new TasksPerUserProjectDto {
                ProjectDto = _mapper.Map<ProjectDto>(kvp.Key),
                TasksCount = kvp.Value
            }).ToList();
        }

        // 2. Получить список тасков, назначенных на конкретного пользователя(по id), где name таска< 45 символов (коллекция из тасков).
        // GET api/<ProjectsController>/TasksPerUserWithNameLengthUpTo45Symbols/5
        [HttpGet("TasksPerUserWithNameLengthUpTo45Symbols/{userId}")]
        public IEnumerable<BsaTaskDto> TasksPerUserWithNameLengthUpTo45Symbols(int userId) {
            var list = _repo.Get()
                .Where(p =>
                    p.Tasks.Any(
                        t => t.Name.Length < 45
                             && t.PerformerId == userId))
                .SelectMany(p => p.Tasks)
                .ToList();
            return _mapper.Map<IEnumerable<BsaTaskDto>>(list);
        }

        // 3. Получить список(id, name) из коллекции тасков, которые выполнены(finished) в текущем(2020) году для конкретного пользователя(по id).
        // GET api/<ProjectsController>/TasksPerUserFinishedIn2020/5
        [HttpGet("TasksPerUserFinishedIn2020/{userId}")]
        public IEnumerable<TasksPerUserFinishedIn2020Dto> TasksPerUserFinishedIn2020(int userId) {
            return _repo.Get()
                .SelectMany(p => p.Tasks)
                .Where(t => DateTime.Compare(t.FinishedAt, DateTime.Now) <= 0
                            && t.PerformerId == userId)
                .Select(task => new TasksPerUserFinishedIn2020Dto(task.Id, task.Name))
                .ToList();
        }

        // 4. Получить список(id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет,
        // отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам.
        //     P.S - в этом запросе допускается проверить только год рождения пользователя, без привязки к месяцу/дню/времени рождения.
        // GET api/<ProjectsController>/CommandsWithUsersOlder10Years
        [HttpGet("CommandsWithUsersOlder10Years")]
        public IEnumerable<CommandsWithUsersOlder10YearsDto> GetCommandsWithUsersOlder10Years() {
            return _repo.Get()
                .Where(proj => proj.Team.TeamMates
                    .Any(mate => DateTime.Compare(mate.Birthday, DateTime.Today.AddYears(-10)) <= 0))
                .Select(p => p.Team)
                .Distinct()
                .Select(t => new CommandsWithUsersOlder10YearsDto(
                    t.Id,
                    t.Name,
                    _mapper.Map<IEnumerable<UserDto>>(
                        t.TeamMates.OrderByDescending(u => u.RegisteredAt) //по дате регистрации пользователя по убыванию - did you mean this? 
                            .ToList())
                )).ToList();
        }

        // 5. Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).
        // GET api/<ProjectsController>/UsersSortedByFirstNameAndTaskNameLength
        [HttpGet("UsersSortedByFirstNameAndTaskNameLength")]
        public IEnumerable<UserDto> GetUsersSortedByFirstNameAndTaskNameLength() {
            var users = _repo.Get()
                .SelectMany(p => p.Tasks)
                .Select(task => task.Performer)
                .Distinct()
                .OrderBy(user => user.FirstName)
                .GroupJoin(_repo.Get().SelectMany(p => p.Tasks),
                    user => user.Id,
                    bsaTask => bsaTask.PerformerId,
                    (user, bsaTaskList) => {
                        bsaTaskList = bsaTaskList.OrderByDescending(task => task.Name.Length);
                        user.Tasks = bsaTaskList.ToList();
                        return user;
                    }).ToList();
            return _mapper.Map<IEnumerable<UserDto>>(users);
        }

        // 6.Получить следующую структуру (передать Id пользователя в параметры):
        // User
        // Последний проект пользователя(по дате создания)
        // Общее кол-во тасков под последним проектом
        // Общее кол-во незавершенных или отмененных тасков для пользователя
        // Самый долгий таск пользователя по дате(раньше всего создан - позже всего закончен)
        // P.S. - в данном случае, статус таска не имеет значения, фильтруем только по дате.
        // GET api/<ProjectsController>/UserAndProjectsDetails/5
        [HttpGet("UserAndProjectsDetails/{userId}")]
        public UserAndProjectsDetailsDto GetUserAndProjectsDetails(int userId) {
            var details = _repo.Get()
                .Where(proj => proj.AuthorId == userId)
                .OrderByDescending(proj => proj.CreatedAt)
                .Take(1)
                .Select(proj => new UserAndProjectsDetailsDto(
                    _mapper.Map<UserDto>(proj.Author),
                    _mapper.Map<ProjectDto>(proj),
                    proj.Tasks.Count,
                    proj.Tasks
                        .Where(t => t.FinishedAt == null)
                        .Select(t => t)
                        .Count(),
                    _mapper.Map<BsaTaskDto>(proj.Tasks
                        .OrderByDescending(t => (t.FinishedAt.Subtract(t.CreatedAt)))
                        .FirstOrDefault())
                )).FirstOrDefault();
            return details;
        }

        //7. Получить следующую структуру:
        // Проект
        // Самый длинный таск проекта(по описанию)
        // Самый короткий таск проекта(по имени)
        // Общее кол-во пользователей в команде проекта, где или описание проекта > 20 символов или кол-во тасков< 3
        // GET api/<ProjectsController>/ProjectWithDetails
        [HttpGet("ProjectWithDetails")]
        public IEnumerable<ProjectWithDetailsDto> GetProjectWithDetails() {
            var projectWithDetails = _repo.Get()
                .Select(pr => {
                    int teamMatesCount = 0;
                    if (pr.Name.Length > 20 || pr.Tasks.Count < 3) {
                        teamMatesCount = pr.Team.TeamMates.Count;
                    }

                    return new ProjectWithDetailsDto(
                        _mapper.Map<ProjectDto>(pr),
                        _mapper.Map<BsaTaskDto>(pr.Tasks
                            .OrderByDescending(t => t.Description.Length)
                            .FirstOrDefault()),
                        _mapper.Map<BsaTaskDto>(pr.Tasks
                            .OrderBy(t => t.Name.Length)
                            .FirstOrDefault()),
                        teamMatesCount
                    );
                })
                .ToList();
            return projectWithDetails;
        }
    }
}