﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ProjectStructure.Model;

namespace ProjectStructure.WebApi.Services {
    public interface IRepository<TEntity> where TEntity : Entity {

        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null);

        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
    }
}