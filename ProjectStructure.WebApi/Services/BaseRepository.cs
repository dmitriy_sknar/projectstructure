﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ProjectStructure.Model;

namespace ProjectStructure.WebApi.Services
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : Entity {
        public List<TEntity> dbContext;
        private IDbInitializer _dbInitializer;
        public BaseRepository(IDbInitializer dbInitializer) {
            _dbInitializer = dbInitializer;
            dbContext = _dbInitializer.Get<TEntity>();
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null) {
            IQueryable<TEntity> query = dbContext.AsQueryable();
            if (filter != null) {
                query = query.Where(filter);
            }

            return query.ToList();
        }

        public void Create(TEntity entity) {
            dbContext.Add(entity);
        }

        public void Update(TEntity entity) {
            var item = dbContext.FirstOrDefault(item => item.Id == entity.Id);
            if (item != null) item = entity;
            else dbContext.Add(entity);
        }

        public void Delete(TEntity entity) {
            dbContext.Remove(entity);
        }
    }
}
