﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.Model;
using AutoMapper;
using ProjectStructure.Model.DTO;
using ProjectStructure.WebApi.DTO;

namespace ProjectStructure.WebApi.Services {
    public class MapConfig : Profile {
        public MapConfig() {
            CreateMap<Project, ProjectDto>();
            // .ForMember(dest => dest.Id, src => src.MapFrom(s => s.Id));
            CreateMap<BsaTask, BsaTaskDto>();
            CreateMap<User, UserDto>();
            CreateMap<Team, TeamDto>();
            CreateMap<TaskState, TaskStateDto>();
        }
    }
}