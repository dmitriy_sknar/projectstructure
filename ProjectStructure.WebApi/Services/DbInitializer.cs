﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ProjectStructure.DAL;
using ProjectStructure.Model;

namespace ProjectStructure.WebApi.Services {
    public class DbInitializer : IDbInitializer {
        private readonly IWebHostEnvironment _env;
        const string DbFileLocation = @"\Files\db.txt";
        const string TasksFileLocation = @"\Files\tasks.txt";
        const string TeamsFileLocation = @"\Files\teams.txt";
        const string UsersFileLocation = @"\Files\users.txt";
        const string ProjectsFileLocation = @"\Files\projects.txt";
        const string StatesFileLocation = @"\Files\states.txt";

        private JsonSerializer serializer;

        private List<Project> _projectDB;
        public List<Project> ProjectDB {
            get
            {
                if (_projectDB == null) InitDb();
                return _projectDB;
            }
            private set => _projectDB = value;
        }

        public List<TEntity> Get<TEntity>() where TEntity : Entity {
            if (typeof(TEntity) == typeof(BsaTask)) 
                return Tasks.Cast<TEntity>().ToList();           
            if (typeof(TEntity) == typeof(TaskState)) 
                return TaskStates.Cast<TEntity>().ToList();            
            if (typeof(TEntity) == typeof(Project)) 
                return ProjectDB.Cast<TEntity>().ToList();            
            if (typeof(TEntity) == typeof(User)) 
                return Users.Cast<TEntity>().ToList();
            if (typeof(TEntity) == typeof(Team))
                return Teams.Cast<TEntity>().ToList();
            return null;
        }

        public DbInitializer(IWebHostEnvironment env) {
            _env = env;
            serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Ignore;
        }

        public void SerializeDb() {
            using StreamWriter sw = new StreamWriter(_env.ContentRootPath + DbFileLocation);
            using JsonWriter writer = new JsonTextWriter(sw);
            serializer.Serialize(writer, ProjectDB);
        }

        public void DeSerializeDb() {
            using StreamReader sr = new StreamReader(_env.ContentRootPath + DbFileLocation);
            using var reader = new JsonTextReader(sr);
            List<Project> db = serializer.Deserialize<List<Project>>(reader);
            ProjectDB = db;
        }

        public void SerializeTasks() {
            using StreamWriter sw = new StreamWriter(_env.ContentRootPath + TasksFileLocation);
            using JsonWriter writer = new JsonTextWriter(sw);
            serializer.Serialize(writer, Tasks);
        }

        public void DeSerializeTasks() {
            using StreamReader sr = new StreamReader(_env.ContentRootPath + TasksFileLocation);
            using var reader = new JsonTextReader(sr);
            List<BsaTask> tasks = serializer.Deserialize<List<BsaTask>>(reader);
            Tasks = tasks;
        }

        public void SerializeTeams() {
            using StreamWriter sw = new StreamWriter(_env.ContentRootPath + TeamsFileLocation);
            using JsonWriter writer = new JsonTextWriter(sw);
            serializer.Serialize(writer, Teams);
        }

        public void DeSerializeTeams() {
            using StreamReader sr = new StreamReader(_env.ContentRootPath + TeamsFileLocation);
            using var reader = new JsonTextReader(sr);
            List<Team> teams = serializer.Deserialize<List<Team>>(reader);
            Teams = teams;
        }

        public void SerializeUsers() {
            using StreamWriter sw = new StreamWriter(_env.ContentRootPath + UsersFileLocation);
            using JsonWriter writer = new JsonTextWriter(sw);
            serializer.Serialize(writer, Users);
        }

        public void DeSerializeUsers() {
            using StreamReader sr = new StreamReader(_env.ContentRootPath + UsersFileLocation);
            using var reader = new JsonTextReader(sr);
            List<User> users = serializer.Deserialize<List<User>>(reader);
            Users = users;
        }

        public void SerializeProjects() {
            using StreamWriter sw = new StreamWriter(_env.ContentRootPath + ProjectsFileLocation);
            using JsonWriter writer = new JsonTextWriter(sw);
            serializer.Serialize(writer, Projects);
        }

        public void DeSerializeProjects() {
            using StreamReader sr = new StreamReader(_env.ContentRootPath + ProjectsFileLocation);
            using var reader = new JsonTextReader(sr);
            List<Project> projects = serializer.Deserialize<List<Project>>(reader);
            Projects = projects;
        }

        public void SerializeTaskStates() {
            using StreamWriter sw = new StreamWriter(_env.ContentRootPath + StatesFileLocation);
            using JsonWriter writer = new JsonTextWriter(sw);
            serializer.Serialize(writer, TaskStates);
        }

        public void DeSerializeTaskStates() {
            using StreamReader sr = new StreamReader(_env.ContentRootPath + StatesFileLocation);
            using var reader = new JsonTextReader(sr);
            List<TaskState> taskStates = serializer.Deserialize<List<TaskState>>(reader);
            TaskStates = taskStates;
        }

        public void InitDb() {
            if (File.Exists(_env.ContentRootPath + DbFileLocation)) {
                DeSerializeTasks();
                DeSerializeTeams();
                DeSerializeUsers();
                DeSerializeProjects();
                DeSerializeTaskStates();
                DeSerializeDb();
            } else {
                InitDbFromWeb();
                SerializeTasks();
                SerializeTeams();
                SerializeUsers();
                SerializeProjects();
                SerializeTaskStates();
                SerializeDb();
            }
        }

        public void InitDbFromWeb() {
            Console.WriteLine("Loading data from API...");
            Console.WriteLine($"Project count loaded: {Projects.Count}");
            Console.WriteLine($"Tasks count loaded: {Tasks.Count}");
            Console.WriteLine($"Task states count loaded: {TaskStates.Count}");
            Console.WriteLine($"Teams count loaded: {Teams.Count}");
            Console.WriteLine($"Users count loaded: {Users.Count} \n");

            ProjectDB = Projects.Select(p => new Project {
                    Id = p.Id,
                    AuthorId = p.AuthorId,
                    TeamId = p.TeamId,
                    CreatedAt = p.CreatedAt,
                    DeadLine = p.DeadLine,
                    Description = p.Description,
                    Name = p.Name,
                })
                .GroupJoin(Tasks,
                    project => project.Id,
                    bsaTask => bsaTask.ProjectId,
                    (project, bsaTaskList) => {
                        bsaTaskList = bsaTaskList.Join(Users,
                            task => task.PerformerId,
                            user => user.Id,
                            (task, user) => {
                                task.Performer = user;
                                return task;
                            });
                        project.Tasks = bsaTaskList.ToList();
                        return project;
                    })
                .Join(Users,
                    project => project.AuthorId,
                    user => user.Id,
                    (project, user) => {
                        project.Author = user;
                        return project;
                    })
                .Join(Teams,
                    project => project.TeamId,
                    team => team.Id,
                    (project, team) => {
                        team.TeamMates = Users
                            .Where(u => u.TeamId == team.Id)
                            .Select(u => u).ToList();
                        project.Team = team;
                        return project;
                    }).ToList();
        }

        private List<Project> _projects;
        public List<Project> Projects {
            get
            {
                if (_projects == null) {
                    _projects = GetProjects().Result;
                }

                return _projects;
            }
            set { _projects = value; }
        }

        private List<BsaTask> _tasks;
        public List<BsaTask> Tasks {
            get
            {
                if (_tasks == null) {
                    _tasks = GetTasks().Result;
                }

                return _tasks;
            }
            set { _tasks = value; }
        }

        private List<TaskState> _taskStates;
        public List<TaskState> TaskStates {
            get
            {
                if (_taskStates == null) {
                    _taskStates = GetTaskStates().Result;
                }

                return _taskStates;
            }
            set { _taskStates = value; }
        }

        private List<Team> _teams;

        public List<Team> Teams {
            get
            {
                if (_teams == null) {
                    _teams = GetTeams().Result;
                }

                return _teams;
            }
            set { _teams = value; }
        }

        private List<User> _users;

        public List<User> Users {
            get
            {
                if (_users == null) {
                    _users = GetUsers().Result;
                }

                return _users;
            }
            set { _users = value; }
        }

        public async Task<List<TaskState>> GetTaskStates() {
            var json = await BsaHttpWebClient.GetTaskStatesAsync();
            List<TaskState> taskStates = JsonConvert.DeserializeObject<List<TaskState>>(json);
            return taskStates;
        }

        public async Task<List<Team>> GetTeams() {
            var json = await BsaHttpWebClient.GetTeamsAsync();
            List<Team> teams = JsonConvert.DeserializeObject<List<Team>>(json);
            return teams;
        }

        public async Task<List<User>> GetUsers() {
            var json = await BsaHttpWebClient.GetUsersAsync();
            List<User> users = JsonConvert.DeserializeObject<List<User>>(json);
            return users;
        }

        public async Task<List<Project>> GetProjects() {
            var json = await BsaHttpWebClient.GetProjectsAsync();
            List<Project> projects = JsonConvert.DeserializeObject<List<Project>>(json);
            return projects;
        }

        public async Task<List<BsaTask>> GetTasks() {
            var json = await BsaHttpWebClient.GetTasksAsync();
            List<BsaTask> tasks = JsonConvert.DeserializeObject<List<BsaTask>>(json);
            return tasks;
        }
    }
}