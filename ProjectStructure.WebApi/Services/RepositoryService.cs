﻿using ProjectStructure.Model;

namespace ProjectStructure.WebApi.Services {
    public class RepositoryService {
        IDbInitializer _dbInitializer;
        public IRepository<BsaTask> Tasks;
        public IRepository<Team> Teams;
        public IRepository<TaskState> TaskStates;
        public IRepository<User> Users;
        public IRepository<Project> Projects;

        public RepositoryService(IDbInitializer dbInitializer,
            IRepository<BsaTask> tasks,
            IRepository<Team> teams,
            IRepository<TaskState> taskStates,
            IRepository<User> users,
            IRepository<Project> projects) {
            _dbInitializer = dbInitializer;
            Tasks = tasks;
            Teams = teams;
            TaskStates = taskStates;
            Users = users;
            Projects = projects;
        }
    }
}