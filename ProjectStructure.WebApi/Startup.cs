using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProjectStructure.Model;
using ProjectStructure.WebApi.Services;

namespace ProjectStructure.WebApi {
    public class Startup {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) {
            services.AddControllers();
            services.AddSingleton<IDbInitializer, DbInitializer>();
            services.AddSingleton<IRepository<BsaTask>, BaseRepository<BsaTask>>();
            services.AddSingleton<IRepository<Team>, BaseRepository<Team>>();
            services.AddSingleton<IRepository<Project>, BaseRepository<Project>>();
            services.AddSingleton<IRepository<User>, BaseRepository<User>>();
            services.AddSingleton<IRepository<TaskState>, BaseRepository<TaskState>>();

            MapperConfiguration mappingConf = new MapperConfiguration(mc => {
                mc.AddProfile(typeof(MapConfig));
            });
            IMapper mapper = mappingConf.CreateMapper();
            services.AddSingleton(mapper);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}