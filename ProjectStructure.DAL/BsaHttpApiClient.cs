﻿using System.Net.Http;
using System.Threading.Tasks;

namespace ProjectStructure.DAL {

    //ToDo make generic httpClient
    
    public class BsaHttpApiClient {
        private const string HOST = "http://localhost:8266";
        private const string PROJECTS_URI = HOST + "/api/projects";
        private const string TASKS_URI = HOST + "/api/tasks";
        private const string TASKSTATES_URI = HOST + "/api/taskstates";
        private const string TEAMS_URI = HOST + "/api/teams";
        private const string USERS_URI = HOST + "/api/users";

        public static async Task<string> GetProjectsAsync() {
            return await GetHttpData(PROJECTS_URI);
        }

        public static async Task<string> GetTasksAsync() {
            return await GetHttpData(TASKS_URI);
        }

        public static async Task<string> GetTaskStatesAsync() {
            return await GetHttpData(TASKSTATES_URI);
        }

        public static async Task<string> GetTeamsAsync() {
            return await GetHttpData(TEAMS_URI);
        }

        public static async Task<string> GetUsersAsync() {
            return await GetHttpData(USERS_URI);
        }

        private static async Task<string> GetHttpData(string uri) {
            using var client = new HttpClient();
            HttpResponseMessage response = (await client.GetAsync(uri)).EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }

        public static async Task<string> GetProjectAsync(int id) {
            return await GetHttpData(PROJECTS_URI + $"/{id}");
        }

        public static async Task<string> GetTasksPerUserProjectDtoAsync(int id) {
            return await GetHttpData(PROJECTS_URI + $"/TasksPerUserProject/{id}");
        }

        public static async Task<string> GetTasksPerUserWithNameLengthUpTo45SymbolsAsync(int id) {
            return await GetHttpData(PROJECTS_URI + $"/TasksPerUserWithNameLengthUpTo45Symbols/{id}");
        }
    }
}